# Hends 2020-2021 C# Eğitimi Proje Kodları

Hacettepe Endüstri ve Sistem Topluluğu tarafından düzenlenmiş olan **C# Eğitimi** proje kodlarını içermektedir.
Eğitmen:  **Burak Şimşek** (burak_simsek@hacettepe.edu.tr)

# Konular

Bu Repository ders sonlarında güncellenecektir. 

![](https://pbs.twimg.com/profile_images/1038413499326754816/NU3QJ2PH_400x400.jpg =100x100)

## 1. Ders - Tanışma & Ders İşleyişi

> **Gereksinimler** 

 - Visual Studio Community 2019 (.Net Paketi ile)
 - Git
 - Tortoise Git
 - Sqlite Browser

