﻿using System;

namespace IkinciHafta
{
    class Program
    {
        static void Main(string[] args)
        {
            // tek satır için...

            /*
            Console.WriteLine("a'nın karesi: " + x);
            Console.WriteLine("b'nin karesi: " + y);
            Console.WriteLine("c'nin karesi: " + c);
            Çoklu yorum satırı
            */
            // Hipotenüs bulma uygulaması
            /*
            int a;
            int b;
            double c;
            Console.WriteLine("Hipotenüs Hesaplama");
            Console.WriteLine("---------------------");
            Console.WriteLine("Lütfen 1. sayıyı giriniz...");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Girdiğiniz 1. sayı: " + a);
            Console.WriteLine("---------------------");
            Console.WriteLine("Lütfen 2. sayıyı giriniz...");
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Girdiğiniz 2. sayı: " + b);
            int x = a * a;   // <----- 3*3 x = a^2 yani x = 9
            int y = b * b;   // <----- 4*4 y = b^2 yani y = 16
            c = x + y;       // <----- c = 9+16 = 25 
            Console.WriteLine("a'nın karesi: " + x);
            Console.WriteLine("b'nin karesi: " + y);
            Console.WriteLine("c'nin karesi: " + c);
            double z = Math.Sqrt(c);
            Console.WriteLine("c (hipotenüs): " + z);
            */

            // Şart Blokları
            // if-else-else if
            /*
            int number = 160;
            if (number>2 || number<5) // 3,4,5,6,7,8,9,10,11,........................ 1 || 0 = 1
            {
                // 4,3,2,1,0,-1,-2,............
                Console.WriteLine("if");
            }
            else if (number>7 && number<12) //8,9,10,11
            {
                Console.WriteLine("else if 1");
            }
            else if (number>13 && number<20) //14,15,16,17,18,19
            {
                Console.WriteLine("else if 2");
            }
            else
            {
                Console.WriteLine("else");
            }
            */

            string username;
            string password;
            Console.WriteLine("Lütfen kullanıcı adınız giriniz...");
            username = Console.ReadLine();

            if (username == "burak")
            {
                Console.WriteLine("Hoşgeldin "+username);
                Console.WriteLine("Lütfen parolanızı giriniz.");
                password = Console.ReadLine();
                if (password == "1234")
                {
                    Console.WriteLine("Parolanız doğru, profil sayfasına yönlendiriliyorsunuz...");
                }
                else
                {
                    Console.WriteLine("Yanlış parola girdiniz. Anasayfaya yönlendiriliyorsunuz...");
                }
            }
            else if (username == "ali")
            {

            }
            else
            {
                Console.WriteLine("Giriş başarısız...");
            }
        }
    }
}
