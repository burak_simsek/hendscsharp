﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hesap_Makinesi
{
    public partial class HesapMakinesiForm : Form
    {
        List<string> sayiListesi = new List<string>();
        string aktifIslem = "yok";
        public HesapMakinesiForm()
        {
            InitializeComponent();
        }

        private void one_Click(object sender, EventArgs e)
        {
           // screen.Text = screen.Text + "1";
            screen.Text += "1";
        }

        private void two_Click(object sender, EventArgs e)
        {
            screen.Text += "2";
        }

        private void three_Click(object sender, EventArgs e)
        {
            screen.Text += "3";
        }

        private void four_Click(object sender, EventArgs e)
        {
            screen.Text += "4";
        }

        private void five_Click(object sender, EventArgs e)
        {
            screen.Text += "5";
        }

        private void six_Click(object sender, EventArgs e)
        {
            screen.Text += "6";
        }

        private void seven_Click(object sender, EventArgs e)
        {
            screen.Text += "7";
        }

        private void eight_Click(object sender, EventArgs e)
        {
            screen.Text += "8";
        }

        private void nine_Click(object sender, EventArgs e)
        {
            screen.Text += "9";
        }
        private void zero_Click(object sender, EventArgs e)
        {
            screen.Text += "0";
        }

        private void close_Click(object sender, EventArgs e)
        {
            screen.Text = "";
            ButonlariAc();
        }

        private void division_Click(object sender, EventArgs e)
        {
            sayiListesi.Add(screen.Text);
            screen.Text = "";
            aktifIslem = "bolme";
        }
        private void multiplication_Click(object sender, EventArgs e)
        {
            sayiListesi.Add(screen.Text);
            screen.Text = "";
            aktifIslem = "carpma";
        }
        private void addition_Click(object sender, EventArgs e)
        {
            sayiListesi.Add(screen.Text);
            screen.Text = "";
            aktifIslem = "toplama";
        }
        private void minus_Click(object sender, EventArgs e)
        {
            sayiListesi.Add(screen.Text);
            screen.Text = "";
            aktifIslem = "cikarma";
        }
        private void equals_Click(object sender, EventArgs e)
        {
            if (aktifIslem=="bolme")
            {
                sayiListesi.Add(screen.Text);
                ButonlariKapat();
                BolmeIslemiYap();
                
            }
            else if (aktifIslem == "carpma")
            {
                sayiListesi.Add(screen.Text);
                ButonlariKapat();
                CarpmaIslemiYap();
            }
            else if (aktifIslem == "toplama")
            {
                sayiListesi.Add(screen.Text);
                ButonlariKapat();
                ToplamaIslemiYap();
            }
            else if (aktifIslem == "cikarma")
            {
                sayiListesi.Add(screen.Text);
                ButonlariKapat();
                CikarmaIslemiYap();
            }
        }
        public void BolmeIslemiYap()
        {
            string birinciSayi = sayiListesi[0];
            string ikinciSayi = sayiListesi[1];
            double intBirinciSayi = double.Parse(birinciSayi);
            double intIkinciSayi = double.Parse(ikinciSayi);
            if (intIkinciSayi==0)
            {
                screen.Text = "Payda 0 olamaz...";
                aktifIslem = "yok";
                sayiListesi.Clear();
            }
            else
            {
                double bolmeSonucu = (intBirinciSayi / intIkinciSayi);
                screen.Text = bolmeSonucu.ToString();
                aktifIslem = "yok";
                sayiListesi.Clear();
            }

        }
        public void CarpmaIslemiYap()
        {
            string birinciSayi = sayiListesi[0];
            string ikinciSayi = sayiListesi[1];
            double intBirinciSayi = double.Parse(birinciSayi);
            double intIkinciSayi = double.Parse(ikinciSayi);

            double carpmaSonucu = (intBirinciSayi * intIkinciSayi);
            screen.Text = carpmaSonucu.ToString();
            aktifIslem = "yok";
            sayiListesi.Clear();
        }
        public void ToplamaIslemiYap()
        {
            string birinciSayi = sayiListesi[0];
            string ikinciSayi = sayiListesi[1];
            double intBirinciSayi = double.Parse(birinciSayi);
            double intIkinciSayi = double.Parse(ikinciSayi);

            double toplamaSonucu = (intBirinciSayi + intIkinciSayi);
            screen.Text = toplamaSonucu.ToString();
            aktifIslem = "yok";
            sayiListesi.Clear();
        }
        public void CikarmaIslemiYap()
        {
            string birinciSayi = sayiListesi[0];
            string ikinciSayi = sayiListesi[1];
            double intBirinciSayi = double.Parse(birinciSayi);
            double intIkinciSayi = double.Parse(ikinciSayi);

            double cikarmaSonucu = (intBirinciSayi - intIkinciSayi);
            screen.Text = cikarmaSonucu.ToString();
            aktifIslem = "yok";
            sayiListesi.Clear();
        }
        public void ButonlariKapat()
        {
            bool status = false;
            zero.Enabled = status;
            one.Enabled = status;
            two.Enabled = status;
            three.Enabled = status; 
            four.Enabled = status;
            five.Enabled = status;
            six.Enabled = status;
            seven.Enabled = status;
            eight.Enabled = status;
            nine.Enabled = status;
            division.Enabled = status;
            multiplication.Enabled = status;
            addition.Enabled = status;
            minus.Enabled = status;
            equals.Enabled = status;
        }
        public void ButonlariAc()
        {
            bool status = true;
            zero.Enabled = status;
            one.Enabled = status;
            two.Enabled = status;
            three.Enabled = status;
            four.Enabled = status;
            five.Enabled = status;
            six.Enabled = status;
            seven.Enabled = status;
            eight.Enabled = status;
            nine.Enabled = status;
            division.Enabled = status;
            multiplication.Enabled = status;
            addition.Enabled = status;
            minus.Enabled = status;
            equals.Enabled = status;
        }


    }
}
