﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KediOyunu
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        int skorDegeri = 0;
        public Form1()
        {
            InitializeComponent();
            skor.Text = skorDegeri.ToString();
            KeyDown += new KeyEventHandler(Form1_KeyDown);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                fish.Top -= 10;
            }
            else if (e.KeyCode == Keys.Down)
            {
                fish.Top += 10;
            }
            else if (e.KeyCode == Keys.Left)
            {
                fish.Left -= 10;
            }
            else if (e.KeyCode == Keys.Right)
            {
                fish.Left += 10;
            }
        }
        public void KurtYe()
        {
            if (fish.Bounds.IntersectsWith(worm.Bounds))
            {
                skorDegeri++;
                skor.Text = skorDegeri.ToString();
                worm.Visible = false;
                
                Point wormPoint = new Point(random.Next(0, ClientRectangle.Width), random.Next(0, ClientRectangle.Height));
                worm.Location = wormPoint;
                worm.Visible = true;
            }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            KurtYe();
        }
    }
}
