﻿namespace KediOyunu
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.fish = new System.Windows.Forms.PictureBox();
            this.worm = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.skor = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.fish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.worm)).BeginInit();
            this.SuspendLayout();
            // 
            // fish
            // 
            this.fish.BackColor = System.Drawing.Color.Transparent;
            this.fish.Image = ((System.Drawing.Image)(resources.GetObject("fish.Image")));
            this.fish.Location = new System.Drawing.Point(140, 166);
            this.fish.Name = "fish";
            this.fish.Size = new System.Drawing.Size(139, 107);
            this.fish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fish.TabIndex = 0;
            this.fish.TabStop = false;
            // 
            // worm
            // 
            this.worm.BackColor = System.Drawing.Color.Transparent;
            this.worm.Image = ((System.Drawing.Image)(resources.GetObject("worm.Image")));
            this.worm.Location = new System.Drawing.Point(341, 193);
            this.worm.Name = "worm";
            this.worm.Size = new System.Drawing.Size(66, 61);
            this.worm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.worm.TabIndex = 1;
            this.worm.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 51);
            this.label1.TabIndex = 2;
            this.label1.Text = "Skor:";
            // 
            // skor
            // 
            this.skor.AutoSize = true;
            this.skor.BackColor = System.Drawing.Color.Red;
            this.skor.Font = new System.Drawing.Font("Comic Sans MS", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.skor.Location = new System.Drawing.Point(139, 9);
            this.skor.Name = "skor";
            this.skor.Size = new System.Drawing.Size(45, 51);
            this.skor.TabIndex = 3;
            this.skor.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1086, 579);
            this.Controls.Add(this.skor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.worm);
            this.Controls.Add(this.fish);
            this.Name = "Form1";
            this.Text = "Balığı Besle";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.fish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.worm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox fish;
        private System.Windows.Forms.PictureBox worm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label skor;
        private System.Windows.Forms.Timer timer1;
    }
}

