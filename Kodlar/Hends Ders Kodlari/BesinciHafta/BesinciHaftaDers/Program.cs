﻿using System;
using System.Collections.Generic;

namespace BesinciHaftaDers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Besinci Hafta Ders Kodlari");
            /*
             // switch - case tekrar
            Console.WriteLine("Lütfen bir meyve adı giriniz...");
            string input = Console.ReadLine();
            input = input.ToLower();
            switch (input)
            {
                case "elma":
                    Console.WriteLine("Girdiginiz meyve "+input);
                    break;
                case "kiraz":
                    Console.WriteLine("Girdiginiz meyve " + input);
                    break;
                case "armut":
                    Console.WriteLine("Girdiginiz meyve " + input);
                    break;
                default:
                    Console.WriteLine("Girdiginiz meyve " + input +" fakat bu meyveyi bilmiyorum.");
                    break;
            }
            */
            /*
            int a, b;
            Console.WriteLine("Aralarında Asal mi ?");
            Console.WriteLine("Birinci sayiyi giriniz: ");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("İkinci sayiyi giriniz: ");
            b = Convert.ToInt32(Console.ReadLine());
            bool durum = true;
            List<int> bolenler = new List<int>();
            if (a>b)
            {
                for (int i = 2; i <= b; i++)
                {
                    if (a%i == 0 && b%i==0)
                    {
                        durum = false;
                        bolenler.Add(i);
                    }
                }
            }
            else if (b>a)
            {
                for (int i = 2; i <= a; i++)
                {
                    if (a % i == 0 && b % i == 0)
                    {
                        durum = false;
                        bolenler.Add(i);
                    }
                }
            }
            else
            {
                durum = false;
            }
            if (durum)
            {
                Console.WriteLine("({0},{1}) sayıları aralarında asaldır.", a, b);
            }
            else
            {
                Console.WriteLine("({0},{1}) sayıları aralarında asal değildir.", a, b);
                Console.Write("Bolen Sayilar Listesi :");
                for (int i = 0; i < bolenler.Count; i++)
                {
                    Console.Write(bolenler[i]+",");
                }
            }*/

            /*
             * void -- > geriye bir değişken döndürmüyor. 
             * int,string, bool, List, float, double ,string[], --> geri bir değişken döndürüyor.
            */

            void Topla(int a, int b)
            {

            }
            void Cikarma(int a, int b)
            {
                Console.WriteLine("Cikarma: " + (a - b));
            }
            int Topla2(int a, int b)
            {
                return a + b;
            }
            /*
            Topla(1, 2);
            Cikarma(1, 2);
            int sonuc = Topla2(1, 2);
            Console.WriteLine("sonuc: " + sonuc);
            */
            
            double IBMCalculator(double boy,double kilo)
            {
                double sonuc = (kilo / Math.Pow(boy, 2));
                return sonuc;
            }
            void IBMCalculator2(double boy, double kilo)
            {
                double sonuc = (kilo / Math.Pow(boy, 2));
            }
            double deger = IBMCalculator(1.50,80);
            if (deger>30.0)
            {
                Console.WriteLine("tlc izle...");
            }
            else
            {
                Console.WriteLine("biraz yemek ye");
            }
            


            string isim;
            IsimGir();// ali
            IsimGir();// veli 
            IsimGir(); // mehmet
           
            Console.WriteLine("sonuc: "+isim);
            
            void IsimGir()
            {
                
                Console.WriteLine("Lütfen bir isim giriniz...");
                isim = Console.ReadLine();
            }
            
            /*
            string IsimGir2()
            {
                Console.WriteLine("Lütfen bir isim giriniz...");
                string girilen = Console.ReadLine();
                return girilen;
            }
            string isim;
            isim = IsimGir2();// ali
            isim = IsimGir2();// veli 
            isim = IsimGir2(); // mehmet
            Console.WriteLine("sonuc: " + isim);

            */
        }
    }
}
