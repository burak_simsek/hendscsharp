﻿using System;

namespace SokagaCikabilirmiyim
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sokaga Cikabilir miyim ?");
            YasakBaslat();
        }
        static void YasakBaslat()
        {
            Console.WriteLine("Yasinizi giriniz...");
            int yas = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Yasiniz: "+yas);
            Console.WriteLine("Saati giriniz...");
            int saat = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Saat: "+saat);
            Console.WriteLine("Mesleğiniz var mı ?(evet,hayir)");
            bool meslek = true;
            string meslek_durumu = Console.ReadLine().ToLower();
            if (meslek_durumu == "evet")
            {
                meslek = true;
                Console.WriteLine("Meslek var.");
            }
            else if (meslek_durumu == "hayir")
            {
                meslek = false;
                Console.WriteLine("Meslek yok.");
            }
            Console.WriteLine("Günü giriniz...(1 hafta içi, 2 hafta sonu)");
            string gun = Console.ReadLine();
            if (gun=="1")
            {
                gun = "hafta_ici";
                Console.WriteLine("Hafta içi");
            }
            else if (gun=="2")
            {
                gun = "hafta_sonu";
                Console.WriteLine("Hafta sonu");
            }
            YasakKontrol(yas, saat, meslek, gun);
        }
        static void YasakKontrol(int yas, int saat, bool meslek, string gun)
        {
            switch (gun)
            {
                case "hafta_ici":
                    if (meslek == true) // çalışıyor
                    {
                        Console.WriteLine("Sen kafana göre takıl...");
                    }
                    else if (meslek == false) // çalışmıyor
                    {
                        if (yas>=65)
                        {
                            if (saat<=13 && saat>=10)
                            {
                                Console.WriteLine("Amca hadi yine iyisin...");
                            }
                            else
                            {
                                Console.WriteLine("Sudoku seni bekler...");
                            }
                        }
                        else if (yas<=20)
                        {
                            if (saat <= 16 && saat >= 13)
                            {
                                Console.WriteLine("Çılgınlarca TikTok çekebilirsin...");
                            }
                            else
                            {
                                Console.WriteLine("Haydi Pc basina...");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Kralsınnn dışarıda gez toz...");
                        }
                    }
                    break;
                case "hafta_sonu":
                    if (saat<=20 && saat>=10)
                    {
                        Console.WriteLine("Hepiniz piknik yapmaya gidebilirsiniz...");
                    }
                    else
                    {
                        Console.WriteLine("Otur evinde beyaaa...");
                    }
                    break;
                default:
                    break;
            }
        }


                    /*
            string durum = "evet";
            do
            {
                YasakBaslat();
                Console.WriteLine("Devam etmek istiyor musunuz? (evet , hayir)");
                durum = Console.ReadLine();
            } while (durum != "hayir");
            */
                    /*
                    int a = 3;
                    int toplam = 0;
                    while (a<10)
                    {
                        toplam = toplam + a;
                        a++;
                    }
                    // 1. adım toplam = 3 , a=4
                    // 2. adım toplam = 7 ,a=5
                    // 3. adım toplam = 12 , a=6
                    // ....
                    // x. adım toplam  y+99  , a=99
                    // a değeri en son 100 
                    Console.WriteLine("sonuc: "+ toplam);
                    */
    }
}
