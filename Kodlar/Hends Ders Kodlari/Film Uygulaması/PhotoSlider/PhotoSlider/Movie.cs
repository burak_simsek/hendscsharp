﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoSlider
{
    class Movie
    {
        public class Film
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Director { get; set; }
            public string MoviePhoto { get; set; }
        }
    }
}
