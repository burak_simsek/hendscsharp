﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static PhotoSlider.Movie;

namespace PhotoSlider
{
    public partial class Form1 : Form
    {
        List<Film> films;
        public Form1()
        {
            InitializeComponent();
            films = new List<Film>();
        }
        public  void FilmListesiYarat(int id,string name,string director, string moviephoto)
        {
            Film film = new Film();
            
            film.Id = id;
            film.Name = name;
            film.Director = director;
            film.MoviePhoto = moviephoto;
            Console.WriteLine(film.MoviePhoto);
            films.Add(film);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void addnewmovie_Click(object sender, EventArgs e)
        {
            FilmListesiYarat(int.Parse(idbox.Text),namebox.Text,directorbox.Text, moviephotobox.Text);
            idbox.Text = "";
            namebox.Text = "";
            directorbox.Text = "";
            moviephotobox.Text = "";
        }
        public void PhotoView(string folder)
        {
            Image myimage = new Bitmap(folder);
            picturemovie.Image = myimage;
        }
        private void getmovielist_Click(object sender, EventArgs e)
        {

            filmcombobox.Items.Clear();

            foreach (var item in films)
            {
                Console.WriteLine(item.Name+" - "+item.Director);
                filmcombobox.Items.Add(item.Name + " - " + item.Director);
            }

        }

        private void filmcombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeMovie(filmcombobox.SelectedIndex);
        }
        public  void ChangeMovie(int selectedId)
        {
            filmname.Text = films[selectedId].Name;
            filmdirector.Text = films[selectedId].Director;
            PhotoView(@"" + films[selectedId].MoviePhoto);
        }
    }
}
