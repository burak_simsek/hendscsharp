﻿using System;

namespace CoffeShop
{
    class Program
    {
        static void Main(string[] args)
        {
            string readCommand = "1";
            do
            {
                Console.WriteLine("Hends Coffee Shop");
                Console.WriteLine("-----------------");
                StartMakeCoffee();
                Console.WriteLine("Press 1 to continue. Press 0 to exit.");
                readCommand = Console.ReadLine();
            } while (readCommand == "1");
        }
        static void StartMakeCoffee()
        {

            int coffeeCups;
            string[] coffeeHardness;
            string tempCoffeeHardness;
            Console.WriteLine("Enter how many cups of coffee you want...");
            coffeeCups = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Coffees soft drink or hard drink?(For example, 6-Hard,4-Soft)");
            tempCoffeeHardness = Console.ReadLine();
            coffeeHardness = tempCoffeeHardness.Split(','); // {"6-Hard","4-Soft"}
            int totalCoffeeWeight = CalculateCoffeeWeight(coffeeHardness);
            Console.WriteLine("----------------------");
            Console.WriteLine("The required weight of coffee is " + totalCoffeeWeight + " grams.\r\n" + "About " + (int)Math.Round(totalCoffeeWeight / 8.0) + " tablespoons or " + (int)Math.Round(totalCoffeeWeight / 3.5) + " teaspoons.");
            int totalCoffeeWater = CalculateCoffeeWater(coffeeCups);
            Console.WriteLine("----------------------");
            Console.WriteLine("The required water of coffee is " + totalCoffeeWater + " milliliters.");
            Console.WriteLine("----------------------");
        }
        static int CalculateCoffeeWeight(string[] _coffeeHardness)
        {
            int softCoffeeWeight = 7;
            int hardCoffeeWeight = 8;
            int coffeWeight  = 0;
            // {"6-Hard","4-Soft"}
            // {"6","Hard"} dizi[0]="6" ,dizi[1]="Hard"
            for (int i = 0; i < _coffeeHardness.Length; i++)
            {
                int coffeeW = int.Parse(_coffeeHardness[i].Split('-')[0]); // int.Parse("6") ==> 6
                string coffeeH = _coffeeHardness[i].Split('-')[1];
                if (coffeeH == "Hard")
                {
                    coffeWeight = coffeWeight + coffeeW * hardCoffeeWeight; //48 gram
                }
                else if (coffeeH == "Soft")
                {
                    coffeWeight = coffeWeight + coffeeW * softCoffeeWeight; // 48 + 4*7 = 76
                }

            }
            return coffeWeight;
        }
        static int CalculateCoffeeWater(int _coffeeCups)
        {
            int defaultCoffeeWater = 125;
            int coffeeWater = 0;
            coffeeWater = _coffeeCups * defaultCoffeeWater;
            return coffeeWater;
        }

    }
}
