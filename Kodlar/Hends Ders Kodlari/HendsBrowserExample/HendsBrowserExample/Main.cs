﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using CefSharp;
using CefSharp.WinForms;
namespace HendsBrowserExample
{
    public partial class Main : MetroForm
    {
        ChromiumWebBrowser browser;
        public Main()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            Cef.EnableHighDPISupport();
            browser = new ChromiumWebBrowser(urlTextBox.Text);
            browserPanel.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
            browser.AddressChanged += Browser_AddressChanged;
            string yazi = "http://google.com";
            string[] parcalanmisYazilar = yazi.Split(':');
            Console.WriteLine(parcalanmisYazilar[0]);
        }
        private void Browser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            urlTextBox.Text = e.Address;
        }
        private void goButton_Click(object sender, EventArgs e)
        {

            if (urlTextBox.Text.Split(':')[0]=="http")
            {
                urlTextBox.BackColor = Color.Red;
                DialogResult dialogResult = MessageBox.Show("Gitmek istediğiniz site güvenli değil !"
                    , "Dikkat",MessageBoxButtons.YesNo);
                if (dialogResult==DialogResult.Yes)
                {
                    browser.Load(urlTextBox.Text);
                }
                else if (dialogResult == DialogResult.No)
                {
                    Close();
                }
            }
            else if (urlTextBox.Text.Split(':')[0] == "https")
            {
                urlTextBox.BackColor = Color.Green;
                browser.Load(urlTextBox.Text);
            }
            
            
        }

        private void urlTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                browser.Load(urlTextBox.Text);
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                browser.Load(urlTextBox.Text);
            }
        }

        private void reloadButton_Click(object sender, EventArgs e)
        {
            browser.Reload();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            if (browser.CanGoBack)
            {
                browser.Back();
            }

        }

        private void forwardButton_Click(object sender, EventArgs e)
        {
            if (browser.CanGoForward)
            {
                browser.Forward();
            }

        }
    }
}
