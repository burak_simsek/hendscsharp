﻿using System;

namespace BirinciOdev
{
    class Program
    {
        static void Main(string[] args)
        {
            // Bu kısımda kullanılan string[] , Split() int.Parse()
            // kavramları ileride detaylı olarak bahsedilecek
            /*
            string[] kitap_isimleri = { "Matematik 1", "Geometri 1", "Coğrafya 2" }; //0,1,2
            Console.WriteLine(kitap_isimleri[0]); // Matematik 1
            Console.WriteLine(kitap_isimleri[1]); // Geometri 1
            Console.WriteLine(kitap_isimleri[2]); // Coğrafya 2
             */
            // Ödev Sorusu 1
            Console.WriteLine("Sıcaklık Dönüştürücü");
            Console.WriteLine("--------------------");
            Console.WriteLine("Önce sıcaklığı giriniz ardından bir boşluk bırakarak büyük harf ile birimi giriniz.");
            Console.WriteLine("Örneğin: 32 F veya 59 C");
            /*
             * 50 F ,23 C 
             */
            Console.Write("Lüften bir sıcaklık değeri ve birimi giriniz: ");
            string girilen_deger;
            girilen_deger = Console.ReadLine();
            string[] degerler = girilen_deger.Split(' '); // boşluk karakterine göre parçaladık
            // Örneğin: 30 F
            // string[] degerler = {"30","F"};
            if (degerler[1]=="F") // F to C
            {
                double c_degeri;
                int f_degeri = int.Parse(degerler[0]); //"30" --> 30
                c_degeri = (f_degeri - 32)/(1.8);
                Console.WriteLine("F -> C : {0} F = {1} C", f_degeri, (int)c_degeri);
            }
            else if (degerler[1]=="C")  // C to F
            {
                double f_degeri;
                int c_degeri = int.Parse(degerler[0]);
                f_degeri = (c_degeri * 1.8) + 32;
                Console.WriteLine("C -> F : {0} C = {1} F", c_degeri, (int)f_degeri);
            }
            // Ödev Sorusu 2
            /*  Muhammed'in ödevinden ekledim.
                Console.Write("Yarıçap Girin:");
                int r = Convert.ToInt32(Console.ReadLine());
                double c = 2 * Math.PI * r;
                Console.WriteLine("Çevre="+ c);
                double a = Math.Pow(r, 2) * Math.PI;
                Console.Write("Alan=" + a); 
             
             */
        }
    }
}
