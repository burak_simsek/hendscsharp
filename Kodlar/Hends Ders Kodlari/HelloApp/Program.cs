﻿using System;

namespace HelloApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // burasi yazi değişkenin tanımlandığı yer (tek satirli yorumlar icin)
            /*
             * cok satirli yorumlar icin
                string yazi = "Selam gencler";
                heyyy
             */
            /*
            2x^2 + 5x + 3 = 0
            3x+2y=2
            2x+y=5
             */
            // İkinci Hafta Dersleri....

            string metin1 = "Selamlar";
            string metin2 = " Nasılsınız?";
            Console.WriteLine(metin1+metin2);
            int sayi1 = 15;
            int sayi2 = 8;
            int toplam = sayi1 + sayi2;
            int carpim = sayi1 * sayi2;
            Console.WriteLine("Toplam: "+toplam);
            Console.WriteLine("Çarpım: " + carpim);
            float sayi4 = 1.5f;
            double sayi5 = 75.9d;
            double sayi6 = 89.789;
            decimal sayi7 = -45678.4546m;
            Console.WriteLine("float: "+sayi4);
            Console.WriteLine("double: " + sayi5);
            Console.WriteLine("double: " + sayi6);
            Console.WriteLine("decimal: " + sayi7);
            char karakter1 = 'd';
            char karakter2 = 't';
            Console.WriteLine(karakter1.ToString()+karakter2.ToString());
            bool bool1 = true;
            bool bool2 = false;
            Console.WriteLine(bool1);
            Console.WriteLine(bool2);
            int yeni_sayi_1 = 10;
            int yeni_sayi_2 = 17;
            // '>' büyüktür '=' eşit '==' eşit ise '<' küçüktür '<=' küçük eşit,'>=' büyük eşit
            bool dogru_mu = false;
            int number = 5;
            if (yeni_sayi_1 > yeni_sayi_2) // true
            {
                Console.WriteLine("evet dogru");
            }
            else if (yeni_sayi_1 < yeni_sayi_2)
            {
                Console.WriteLine("hayir yanlis");
            }
            // İkinci Hafta Dersleri.....
            /*
            string yazi = "Selam gencler";
            string yazi2 = "merhaba";
            Console.WriteLine(yazi);
            Console.WriteLine(yazi2);
            int sayi1 = 5;
            int sayi2 = 4;
            int sayi3 = 9;
            int sayi4 = sayi3+5; 
            Console.WriteLine(sayi3);
            Console.WriteLine(sayi4);
            int sayi6 = 213647;
            float sayi7 = 1.5f;
            double sayi8 = 58.9d;
            decimal sayi9 = -785.2354m;
            Console.WriteLine(sayi7);
            Console.WriteLine(sayi8+sayi4);
            Console.WriteLine(sayi9);
            char char1 = 'c';
            char char_two = (char)106;
            // c+# = c#
            Console.WriteLine(char1.ToString()+'#'.ToString());
            Console.WriteLine(char_two);

            bool bool1 = true;
            bool bool2 = false;
            Console.WriteLine(bool1);
            Console.WriteLine(bool2);
            int yeni_sayi_1 = 10;
            int yeni_sayi_2 = 7;
            // '>' büyüktür '=' eşit '==' eşit ise '<' küçüktür '<=' küçük eşit,'>=' büyük eşit
            bool dogru_mu = false;
            int number = 5;
            if (yeni_sayi_1 > yeni_sayi_2) // true
            {
                Console.WriteLine("evet dogru");
            }
            else if(yeni_sayi_1 < yeni_sayi_2)
            {
                Console.WriteLine("hayir yanlis");
            }
            */
        }
    }
}
