﻿using System;
using System.Collections.Generic;

namespace DorduncuHafta
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("-------------");
            string meyve;
            Console.WriteLine("Lütfen bir meyve adı giriniz...");
            meyve = Console.ReadLine();
            switch (meyve)
            {
                case "armut":
                    Console.WriteLine("Selam armut");
                    break;
                case "mandalina":
                    Console.WriteLine("Selam mandalina");
                    break;
                case "elma":
                    Console.WriteLine("Selam elma");
                    break;
                default:
                    Console.WriteLine("Selam sen kimsin ?");
                    break;
            }
            */
            /*
                    Console.WriteLine("-------------");
                    string gun;
                    Console.WriteLine("Lütfen günü giriniz...");
                    gun = Console.ReadLine();
                    switch (gun)
                    {
                        case "Pazartesi":
                            Console.WriteLine("Haftanın 1. günü");
                            break;
                        case "Sali":
                            Console.WriteLine("Haftanın 2. günü");
                            break;
                        case "Carsamba":
                            Console.WriteLine("Haftanın 3. günü");
                            break;
                        default:
                            Console.WriteLine(gun+ " adlı gün haftanın hangi günü hiç bilmiyorum :(");
                            break;
                    }
            */
            /*
            int sum=0;
            for (int i=1; i<=10;i++)
            {
                sum = sum +i;
            }
            
            Console.WriteLine("Toplam = "+sum);
            */
            /*
            for (int i =10;i>=0;i--)
            {
                if (i%2==0) // çift sayiyi bulalım
                {
                    Console.WriteLine("{0} sayisi cift sayidir.",i);
                }
                else if (i%2==1) // tek sayiyi bulalım
                {
                    Console.WriteLine("{0} sayisi tek sayidir.",i);
                }
            }
            */
            /*
            int counter = 0;
            int girilen_sayi;

            Console.WriteLine("Lütfen bir sayi giriniz...");
            girilen_sayi = Convert.ToInt32(Console.ReadLine());
            //int[] bolen_sayilar = new int[girilen_sayi];
            List<int> bolen_sayilar_listesi = new List<int>();
            bolen_sayilar_listesi.Add(1);
            for (int i = 2; i < girilen_sayi; i++)
            {
                if (girilen_sayi % i == 0)
                {
                    bolen_sayilar_listesi.Add(i);
                   // Console.WriteLine(girilen_sayi + " , " + i + " sayisina tam bolunur");
                    counter++;
                }

            }
            bolen_sayilar_listesi.Add(girilen_sayi);
            if (counter == 0)
            {
                Console.WriteLine(girilen_sayi + " asal sayıdır.");
            }
            else
            {
                int toplam_bolen_sayisi = counter + 2;  // kendisi ve bir dahil
                Console.WriteLine(girilen_sayi + " asal sayı değildir.");
                Console.WriteLine(girilen_sayi + " sayısının " + toplam_bolen_sayisi + " böleni vardır.");
            }
            for (int i = 0; i < bolen_sayilar_listesi.Count; i++)
            {
                Console.WriteLine(bolen_sayilar_listesi[i]);
            }
            */
            //string[] cards = { "Clubs", "Hearts", "Diamonds", "Spades" };

            /*
              string[] dizi = { "asd" };
              string[] dizi2 = new string[4]; //0,1,2,3
              dizi[0] = "heyoo"; // dizinin 1. elemanı
            */
            /*
            string[] cards = { "Sinek", "Kupa", "Karo", "Maca" };
            string[] card_name = {
                "As", "2", "3", "4","5",
                "6","7","8","9","10",
                "Vale","Kız","Papaz"
                            }; // 13 elemanlı

            for (int i = 0; i < cards.Length; i++)
            {
                for (int j = 0; j < card_name.Length; j++)
                {
                    Console.WriteLine(cards[i] + " - " + card_name[j]);
                }
                Console.WriteLine("---------------");
            }

            */
            /*
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    for (int k = 0; k < 10; k++)
                    {
                        Console.WriteLine(i +"/"+j+"/"+k);
                    }
                }
            }
            */
            string tekrar;
            do
            {
                // PrimeNumber();
                List<int> _bolenler = new List<int>();
                Console.WriteLine("İlk sayiyi giriniz...");
                long a = Convert.ToInt64(Console.ReadLine());
                Console.WriteLine("İkinci sayiyi giriniz...");
                long b = Convert.ToInt64(Console.ReadLine());
                bool sonuc = AralarindaAsal(ref _bolenler,a,b);
                if (sonuc == true)
                {
                    Console.WriteLine("Aralarında asaldır.");

                }
                else
                {
                    Console.WriteLine("Aralarında asal değildir..");
                    Console.WriteLine($"({a},{b}) sayilari tam bölen ortak sayilar asagidaki gibidir.");
                    Console.WriteLine("------------------------------");
                    foreach (var item in _bolenler)
                    {
                        Console.WriteLine($"{item}");
                    }
                    Console.WriteLine("------------------------------");
                }
               // Console.WriteLine("Tekrar denemek için  \"1\" çıkış için \"0\" yazın.");
               // tekrar = Console.ReadLine();
                tekrar = "1";
                if (tekrar != "1")

                    Console.WriteLine();

            } while (tekrar == "1");


        }
        static void PrimeNumber()
        {
            int counter = 0;
            int girilen_sayi;

            Console.WriteLine("Lütfen bir sayi giriniz...");
            girilen_sayi = Convert.ToInt32(Console.ReadLine());
            //int[] bolen_sayilar = new int[girilen_sayi];
            List<int> bolen_sayilar_listesi = new List<int>();
            bolen_sayilar_listesi.Add(1);
            for (int i = 2; i < girilen_sayi; i++)
            {
                if (girilen_sayi % i == 0)
                {
                    bolen_sayilar_listesi.Add(i);
                    // Console.WriteLine(girilen_sayi + " , " + i + " sayisina tam bolunur");
                    counter++;
                }

            }
            bolen_sayilar_listesi.Add(girilen_sayi);
            if (counter == 0)
            {
                Console.WriteLine(girilen_sayi + " asal sayıdır.");
            }
            else
            {
                int toplam_bolen_sayisi = counter + 2;  // kendisi ve bir dahil
                Console.WriteLine(girilen_sayi + " asal sayı değildir.");
                Console.WriteLine(girilen_sayi + " sayısının " + toplam_bolen_sayisi + " böleni vardır.");
            }
            for (int i = 0; i < bolen_sayilar_listesi.Count; i++)
            {
                Console.WriteLine(bolen_sayilar_listesi[i]);
            }
        }
        static bool AralarindaAsal(ref List<int> bolenler, long a, long b)
        {

            bool durum = true;
            if (a >= b)
            {
                for (int i = 2; i <= b; i++)
                {
                    if (a % i == 0 && b % i == 0)
                    {
                        bolenler.Add(i);
                        durum = false;
                    }
                }
            }
            else if (a < b)
            {
                for (int i = 2; i <= a; i++)
                {
                    if (a % i == 0 && b % i == 0)
                    {
                        bolenler.Add(i);
                        durum = false;
                    }
                }
            }

            return durum;
        }
    }
}
