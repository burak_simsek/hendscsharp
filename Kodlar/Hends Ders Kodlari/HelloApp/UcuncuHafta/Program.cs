﻿using System;
using System.Numerics;

namespace UcuncuHafta
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---Not Ortalaması Hesaplama---");
            /*
             2 vize + 1 final
             vizelerin %30 u finalin %40
             geçer harf notu D olsun.
             A1 -> 95--100
             A2 -> 90--95
             A3 -> 85--90
             B1 -> 80--85
             B2 -> 75--80
             B3 -> 70--75
             C1 -> 65--70
             C2 -> 60--65
             C3 -> 55--60
             D  -> 50--55
             F  ->  <50 (Kaldınız.)
             */
            int vize1, vize2, final;

            Console.WriteLine("----------------------------------");
            Console.WriteLine("Lütfen 1.Vize Notunuzu giriniz: ");
            vize1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("----------------------------------");
            Console.WriteLine("Lütfen 2.Vize Notunuzu giriniz: ");
            vize2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("----------------------------------");
            Console.WriteLine("Lütfen Final Notunuzu giriniz: ");
            final = Convert.ToInt32(Console.ReadLine());
            double ortalama;
            if (vize1 <= 0 || vize2 <= 0 || final <= 0) // 50,60,70
            {
                Console.WriteLine("Ne kadar tembelsin. Bu notları almayı nasıl başardın...");
            }
            else
            {
                ortalama = (vize1 * 3 / 10) + (vize2 * 3 / 10) + (final * 4 / 10);
                Console.WriteLine("Ders ortalamanız: "+ortalama);
                SonucGetir(ortalama); // 70.5
            }
        }

        static void SonucGetir(double sonuc)
        {
            int yeni_sonuc = (int)sonuc;// integer a cast ettik
            if (yeni_sonuc <= 100 && yeni_sonuc > 95 )  // (95-100]    95<yeni_sonuc<=100
            {
                Console.WriteLine("Harf notunuz A1");
            }
            else if(yeni_sonuc <= 95 && yeni_sonuc > 90) // (90,95]   90<yeni_sonuc<=95
            {
                Console.WriteLine("Harf notunuz A2");
            }
            else if (yeni_sonuc <= 90 && yeni_sonuc > 85) // (85,90]
            {
                Console.WriteLine("Harf notunuz A3");
            }
            else if (yeni_sonuc <= 85 && yeni_sonuc > 80)
            {
                Console.WriteLine("Harf notunuz B1");
            }
            else if (yeni_sonuc <= 80 && yeni_sonuc > 75)
            {
                Console.WriteLine("Harf notunuz B2");
            }
            else if (yeni_sonuc <= 75 && yeni_sonuc > 70)
            {
                Console.WriteLine("Harf notunuz B3");
            }
            else if (yeni_sonuc <= 70 && yeni_sonuc > 65)
            {
                Console.WriteLine("Harf notunuz C1");
            }
            else if (yeni_sonuc <= 65 && yeni_sonuc > 60)
            {
                Console.WriteLine("Harf notunuz C2");
            }
            else if (yeni_sonuc <= 60 && yeni_sonuc > 55)
            {
                Console.WriteLine("Harf notunuz C3");
            }
            else if (yeni_sonuc <= 55 && yeni_sonuc > 50)
            {
                Console.WriteLine("Harf notunuz D");
            }
            else if (yeni_sonuc < 50) //45 aldıysa dersten kaldı.
            {
                Console.WriteLine("Kaldınız...");
            }
        }
    }
}
