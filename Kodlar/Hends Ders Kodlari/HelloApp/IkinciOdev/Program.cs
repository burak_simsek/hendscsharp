﻿using System;

namespace IkinciOdev
{
    class Program
    {
        public static void Main()
        {
            string tekrar;
            do
            {

                //Zeller();
                //IkinciDereceDenklem(5,3,-2);
                Console.WriteLine("Tekrar denemek için  \"evet\" çıkış için \"hayır\" yazın.");
                tekrar = Console.ReadLine();
                if (tekrar != "evet")

                    Console.WriteLine();

            } while (tekrar == "evet");

        }
        static void Zeller()
        {
            int gun, ay, yil;
            Console.WriteLine("Zeller Algoritması");
            Console.WriteLine("------------------");
            Console.Write("DD Formatında Gün'ü giriniz(Örnegin: 01,10,15,...): ");
            gun = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("------------------");
            Console.Write("MM Formatında Ay'ı giriniz(Örnegin: 01,09,11,...): ");
            ay = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("------------------");
            Console.Write("YYYY Formatında Yıl'ı giriniz(Örnegin: 1990,2000,1881,...): ");
            yil = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Girdiğiniz tarih: {0}/{1}/{2}", gun, ay, yil);
            if (ay == 1)
            {
                ay = 13;
                yil--;
            }
            if (ay == 2)
            {
                ay = 14;
                yil--;
            }
            int q = gun;
            int m = ay;
            int k = yil % 100;
            int j = yil / 100;
            int h = q + (13 * (m + 1) / 5) + k + (k / 4) + (j / 4) + (5 * j);
            h = h % 7;
            getDay(h);

        }
        static void getDay(int h)
        {
            if (h == 0)
            {
                Console.WriteLine("Cumartesi");
            }
            else if (h == 1)
            {
                Console.WriteLine("Pazar");
            }
            else if (h == 2)
            {
                Console.WriteLine("Pazartesi");
            }
            else if (h == 3)
            {
                Console.WriteLine("Salı");
            }
            else if (h == 4)
            {
                Console.WriteLine("Çarşamba");
            }
            else if (h == 5)
            {
                Console.WriteLine("Perşembe");
            }
            else if (h == 6)
            {
                Console.WriteLine("Cuma");
            }
        }

        static void IkinciDereceDenklem(int a, int b, int c)
        {
            Console.WriteLine("denkelem kökleri bulma");

            Console.WriteLine("denklem");
            Console.WriteLine("(" + a + ")" + "x²+(" + b + ")x+(" + c + ")");

            double x1;
            double x2;
            double delta = b * b - 4 * a * c;
            Console.WriteLine("Delta: {0}", delta);
            if (delta > 0)
            {
                x1 = (-1 * b - Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
                x2 = (-1 * b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
                Console.WriteLine("1.kök");
                Console.WriteLine(x1);
                Console.WriteLine("2.kök");
                Console.WriteLine(x2);
                Console.WriteLine("kökler toplamı");
                Console.WriteLine(x1 + x2);
                Console.WriteLine("kökler çarpımı");
                Console.WriteLine(x1 * x2);
            }

        }
        /*
h = ((h + 5)) % 7 + 1;
if (h == 1)
{
    Console.WriteLine("Pazartesi");
}
else if (h == 2)
{
    Console.WriteLine("Salı");
}
else if (h == 3)
{
    Console.WriteLine("Çarşamba");
}
else if (h == 4)
{
    Console.WriteLine("Perşembe");
}
else if (h == 5)
{
    Console.WriteLine("Cuma");
}
else if (h == 6)
{
    Console.WriteLine("Cumartesi");
}
else if (h == 7)
{
    Console.WriteLine("Pazar");
}
*/
    }
}
