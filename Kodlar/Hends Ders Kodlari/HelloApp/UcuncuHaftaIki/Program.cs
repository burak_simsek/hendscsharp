﻿using System;

namespace UcuncuHaftaIki
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Koordinat Sistemi Uygulaması");
            Console.WriteLine("-----------------------------");
            int x, y;
            Console.WriteLine("-----------------------------");
            Console.WriteLine("Apsis'i giriniz: ");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-----------------------------");
            Console.WriteLine("Ordinat'ı giriniz: ");
            y = Convert.ToInt32(Console.ReadLine());
            /*
             x>0 ve y>0 ise 1. bölgede -> x>0 && y>0
             x<0 ve y>0 ise 2. bölgede -> x<0 && y>0
             x<0 ve y<0 ise 3. bölgede -> x<0 && y<0
             x>0 ve y<0 ise 4. bölgede -> x>0 && y<0
             x=0 ve y=0 ise merkezde   -> x==0 && y==0
             */
            if (x > 0 && y > 0)
            {
                Console.WriteLine("({0},{1}) 1. Bölgede",x,y);
                Console.WriteLine("("+x+","+y+")"+" 1. Bölgede");
            }
            else if (x < 0 && y > 0)
            {
                Console.WriteLine("({0},{1}) 2. Bölgede", x, y);
            }
            else if (x < 0 && y < 0)
            {
                Console.WriteLine("({0},{1}) 3. Bölgede", x, y);
            }
            else if (x > 0 && y < 0)
            {
                Console.WriteLine("({0},{1}) 4. Bölgede", x, y);
            }
            else if (x == 0 && y == 0)
            {
                Console.WriteLine("({0},{1}) orijin(merkezde)", x, y);
            }
        }
    }
}
