﻿using Kitaplik.Modeller;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitaplik.Fonksiyonlar
{
    class Sql
    {
        public static void Post(string komut)
        {
            try
            {
                string data = komut; // Insert into user .....
                SQLiteConnection con = new SQLiteConnection("Data Source=kitaplik.db;Version=3;");
                con.Open();
                try
                {
                    using (SQLiteCommand mycommand = new SQLiteCommand(con))
                    {
                        mycommand.CommandText = data; //Insert into user .....
                        mycommand.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e);
                }

                con.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
            }
        }
        public static void UserData(string query, ref List<User> userList)
        {
            try
            {
                string data = query;
                SQLiteConnection con = new SQLiteConnection("Data Source=kitaplik.db;Version=3;");
                con.Open();
                try
                {
                    using (SQLiteCommand mycommand = new SQLiteCommand(con))
                    {

                        mycommand.CommandText = data;
                        using (SQLiteDataReader rdr = mycommand.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                User _user = new User();
                                _user.ID = rdr.GetInt32(0);
                                _user.Username = rdr.GetString(1);
                                _user.Password = rdr.GetString(2);
                                _user.Type = rdr.GetString(3);
                                _user.Status = bool.Parse(rdr.GetString(4));
                                userList.Add(_user);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e);
                }
                con.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
            }

        }
        public static void BookData(string query, ref List<Book> bookList)
        {
            try
            {
                string data = query;
                SQLiteConnection con = new SQLiteConnection("Data Source=kitaplik.db;Version=3;");
                con.Open();
                try
                {
                    using (SQLiteCommand mycommand = new SQLiteCommand(con))
                    {

                        mycommand.CommandText = data;
                        using (SQLiteDataReader rdr = mycommand.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                Book _book = new Book();
                                _book.ID = rdr.GetInt32(0);
                                _book.Name = rdr.GetString(1);
                                _book.Author = rdr.GetString(2);
                                _book.Category = rdr.GetString(3);
                                _book.Publisher = rdr.GetString(4);
                                _book.PagesCount = rdr.GetInt32(5);
                                _book.ReleaseDate = rdr.GetString(6);
                                _book.ISBN = rdr.GetString(7);
                                bookList.Add(_book);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e);
                }
                con.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
            }

        }
        public static void CategoryData(string query, ref List<Category> categoryList)
        {
            try
            {
                string data = query;
                SQLiteConnection con = new SQLiteConnection("Data Source=kitaplik.db;Version=3;");
                con.Open();
                try
                {
                    using (SQLiteCommand mycommand = new SQLiteCommand(con))
                    {

                        mycommand.CommandText = data;
                        using (SQLiteDataReader rdr = mycommand.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                Category _category = new Category(rdr.GetInt32(0), rdr.GetString(1));
                                categoryList.Add(_category);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e);
                }
                con.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
            }

        }
    }
}
