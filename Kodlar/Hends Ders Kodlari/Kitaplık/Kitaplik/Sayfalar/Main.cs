﻿using Kitaplik.Fonksiyonlar;
using Kitaplik.Modeller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kitaplik.Sayfalar
{

    public partial class Main : Form
    {

        public Main(User user)
        {
            InitializeComponent();
        }
        public Main()
        {
            InitializeComponent();
        }

        private void addNewCategory_Click(object sender, EventArgs e)
        {
            if (categoryTextBox.TextLength > 0)
            {
                Console.WriteLine(categoryTextBox.Text);
                List<Category> categoryList = new List<Category>();
                string categoryQuery = $"SELECT * FROM category WHERE name='{categoryTextBox.Text}';";
                Sql.CategoryData(categoryQuery, ref categoryList);
                if (categoryList.Count() > 0)
                {
                    MessageBox.Show("Eklemek istediğiniz kategori zaten mevcut.", "Hata");
                }
                else
                {
                    string query = $"INSERT INTO category(name) values('{categoryTextBox.Text}');";
                    Sql.Post(query);
                    UpdateCategory();
                }

            }
            else
            {
                MessageBox.Show("Kategori adını boş bıraktınız.", "Hata");
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            UpdateCategory();
        }
        public void UpdateCategory()
        {
            categoryComboBox.Items.Clear();
            List<Category> categoryList = new List<Category>();
            string categoryQuery = $"SELECT * FROM category;";
            Sql.CategoryData(categoryQuery, ref categoryList);
            foreach (var item in categoryList)
            {
                categoryComboBox.Items.Add(item.Name);
            }
        }

        private void saveBook_Click(object sender, EventArgs e)
        {
            List<Book> bookList = new List<Book>();
            Sql.BookData($"Select * From book Where name='{bookNameTextBox.Text}';",ref bookList);
            if (bookList.Count()>0)// demek ki eklemek istediğim kitap veritabanında var.
            {
                MessageBox.Show($"Eklemek istediğini {bookNameTextBox.Text} isimli kitap zaten mevcut.", "Hata");
            }
            else // demek ki eklemek istediğim kitap veritabanında yok.
            {
                string query = $"INSERT INTO book(name,author,category,publisher,pagesCount,releaseDate,isbn) " +
                               $"values('{bookNameTextBox.Text}','{bookAuthorTextBox.Text}','{categoryComboBox.SelectedItem}'" +
                               $",'{bookPublisherTextBox.Text}',{int.Parse(bookPagescountTextBox.Text)},'{bookReleasedateTextBox.Text}'" +
                               $",'{bookIsbnTextBox.Text}');";
                Console.WriteLine(query);
                Sql.Post(query);
                bookNameTextBox.Text = "";
                bookAuthorTextBox.Text = "";
                bookPublisherTextBox.Text = "";
                bookPagescountTextBox.Text = "";
                bookReleasedateTextBox.Text = "";
                bookIsbnTextBox.Text = "";
            }


        }

        private void categoryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("seçilen kategori: " + categoryComboBox.SelectedItem);
        }
    }
}
