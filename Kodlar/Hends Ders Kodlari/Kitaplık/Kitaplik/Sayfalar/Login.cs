﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kitaplik.Modeller;
using Kitaplik.Fonksiyonlar;
namespace Kitaplik.Sayfalar
{
    public partial class Login : Form
    {
        List<User> users = new List<User>();
        List<Book> books = new List<Book>();
        public Login()
        {
            InitializeComponent();
            Console.WriteLine("-------------Users List---------");
            Sql.UserData("SELECT * FROM user;",ref users);
            foreach (var item in users)
            {
                Console.WriteLine(item.Username);
            }
            Console.WriteLine("-------------Books List---------");
            Sql.BookData("SELECT * FROM book;", ref books);
            foreach (var item in books)
            {
                Console.WriteLine(item.Name + " / "+item.Author);
            }

        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            // Select * From User Where username="faruk" AND password="gizli";
            if (usernameBox.TextLength > 0 && passwordBox.TextLength > 0)
            {
                List<User> userLogin = new List<User>();
                string query = $"SELECT * FROM user WHERE username='{usernameBox.Text}' AND password='{passwordBox.Text}';";
                Console.WriteLine(query);
                Sql.UserData(query, ref userLogin);
                if (userLogin.Count()>0)// Demek ki eşleşme var. Yani 1 Adet kullanıcı var.
                {
                    
                    MessageBox.Show($"Merhaba {userLogin[0].Username}, giriş işleminiz başarılı.", "Başarılı");
                    Main mainForm = new Main(userLogin[0]);
                    mainForm.FormClosed += new FormClosedEventHandler(mainForm_FormClosed);
                    mainForm.Show();
                    this.Hide();
                }
                else // Demek ki eşleşme yok.
                {
                    MessageBox.Show($"Merhaba {usernameBox.Text}, sisteme kayıtlı değilsiniz.", "Hata");
                }
            }
            else
            {
                MessageBox.Show("Kullanıcı adınız veya şifrenizi boş bıraktınız.", "Hata");
            }
        }
        private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void kayitOlButton_Click(object sender, EventArgs e)
        {

        }
    }
}
