﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitaplik.Modeller
{
    public class Book
    {
        private int _ID;
        private string _Name;
        private string _Author;
        private string _Category;
        private string _Publisher;
        private int _PagesCount;
        private string _ReleaseDate;
        private string _ISBN;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        public string Author
        {
            get
            {
                return _Author;
            }
            set
            {
                _Author = value;
            }
        }
        public string Category
        {
            get
            {
                return _Category;
            }
            set
            {
                _Category = value;
            }
        }
        public string Publisher
        {
            get
            {
                return _Publisher;
            }
            set
            {
                _Publisher = value;
            }
        }
        public int PagesCount
        {
            get
            {
                return _PagesCount;
            }
            set
            {
                _PagesCount = value;
            }
        }
        public string ReleaseDate
        {
            get
            {
                return _ReleaseDate;
            }
            set
            {
                _ReleaseDate = value;
            }
        }
        public string ISBN
        {
            get
            {
                return _ISBN;
            }
            set
            {
                _ISBN = value;
            }
        }
        public Book()
        {

        }
    }
}
