﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitaplik.Modeller
{
    public class Category
    {
        private int _ID;
        private string _Name;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        public Category(int _id, string _name)
        {
            _ID = _id;
            _Name = _name;
        }
    }
}
