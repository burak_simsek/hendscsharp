﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitaplik.Modeller
{
    public class User
    {
        private int _ID;
        private string _Username;
        private string _Password;
        private string _Type;
        private bool _Status;

        // Encapsulation işlemi
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                _Username = value;
            }
        }
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }
        public string Type
        {
            get
            {
                return _Type;

            }
            set
            {
                _Type = value;
            }
        }
        public bool Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }

        public User(int _id, string _username, string _password, string _type, bool _status)
        {
            _ID = _id;
            _Username = _username;
            if (_password.Length > 4)
            {
                _Password = _password;
            }
            _Type = _type;
            _Status = _status;
        }
        public User()
        {

        }
    }
}
