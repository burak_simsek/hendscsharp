﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Siniflar1
{
    class Emlak
    {
        private string _Adres;
        private string _Cephe;
        private int _Fiyat;
        private bool _Satici = false; //Sahibi satıyor
        private string _Tip;
        private int _Kat;
        private int _Alan;
        EvOzellik _evOzellik = new EvOzellik();
        public EvOzellik evOzellik
        {
            get
            {
                return _evOzellik;
            }
            set
            {

                _evOzellik = value;
                evOzellik = _evOzellik;
            }
        }
        public class EvOzellik
        {
            public string opt1;
            public string opt2;
            public string opt3;
            public string opt4;

        }
        public string Adres
        {
            get
            {
                return _Adres;
            }
            set
            {
                _Adres = value;
            }
        }
        public string Cephe
        {
            get
            {
                return _Cephe;
            }
            set
            {
                _Cephe = value;
            }
        }
        public int Fiyat
        {
            get
            {
                int sonFiyat = _Fiyat;
                if (_Satici == false)//satici olan kişi emlakçı değil. Sahibi
                {
                    sonFiyat = _Fiyat;
                }
                else if (_Satici == true)// satici olan kişi emlakçı. Emlakçı
                {
                    sonFiyat = (int)(_Fiyat - (_Fiyat * 0.2));
                }
                return sonFiyat;
            }
            set
            {
                _Fiyat = value;

            }
        }
        public bool Satici
        {
            get
            {
                return _Satici;
            }
            set
            {
                _Satici = value;
            }
        }
        public string Tip
        {
            get
            {
                return _Tip;
            }
            set
            {
                _Tip = value;
            }
        }
        public int Kat
        {
            get
            {
                return _Kat;
            }
            set
            {
                _Kat = value;
            }
        }
        public int Alan
        {
            get
            {
                return _Alan;
            }
            set
            {
                _Alan = value;
            }
        }
    }

}
