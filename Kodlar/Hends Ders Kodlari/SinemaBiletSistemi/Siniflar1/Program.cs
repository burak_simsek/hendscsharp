﻿using System;

namespace Siniflar1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Emlak Projesi");
            Emlak emlak = new Emlak();
            emlak.Adres = "Ankara";
            emlak.Cephe = "Güney";
            emlak.Alan = 150;
            emlak.Fiyat = 2000;
            emlak.Kat = 5;
            emlak.Satici = false;
            emlak.Tip = "2+1";
            Console.WriteLine($"Bu evin fiyatı {emlak.Fiyat} TL. Ve Satıcısı {(emlak.Satici == false ? "Kendisi" : "Emlakci")}");
            emlak.Satici = true;
            Console.WriteLine($"Bu evin fiyatı {emlak.Fiyat} TL. Ve Satıcısı {(emlak.Satici == false ? "Kendisi" : "Emlakci")}");
            
            emlak.evOzellik.opt1 = "Merkezi sistem ısıtması mevcut.";
            emlak.evOzellik.opt2 = "123";
            emlak.evOzellik.opt3 = "Toplu taşıma araçlarına 10 dk yürüme mesafesinde.";
            emlak.evOzellik.opt4 = "Spor salonu var.";
            Console.WriteLine(emlak.evOzellik.opt4);
            
        }
    }
}
