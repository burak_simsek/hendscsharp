﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinemaBiletSistemi
{
    class Araba
    {
        public string Marka;
        public string Model;
        public string Yil;
        public int Fiyat;
        public string Renk;
        private bool Durum;

        public bool ArabaDurumu
        {
            get { return Durum; }
            set { Durum = value; }
        }
        public Araba(string _Marka, string _Model)
        {
            Marka = _Marka;
            Model = _Model;
        }
        public Araba()
        {

        }
    }
}
