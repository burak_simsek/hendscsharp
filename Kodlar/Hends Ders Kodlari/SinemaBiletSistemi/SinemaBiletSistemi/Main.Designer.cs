﻿
namespace SinemaBiletSistemi
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.seat1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.seat14 = new System.Windows.Forms.Button();
            this.seat13 = new System.Windows.Forms.Button();
            this.seat12 = new System.Windows.Forms.Button();
            this.seat8 = new System.Windows.Forms.Button();
            this.seat7 = new System.Windows.Forms.Button();
            this.seat6 = new System.Windows.Forms.Button();
            this.seat5 = new System.Windows.Forms.Button();
            this.seat11 = new System.Windows.Forms.Button();
            this.seat10 = new System.Windows.Forms.Button();
            this.seat9 = new System.Windows.Forms.Button();
            this.seat4 = new System.Windows.Forms.Button();
            this.seat3 = new System.Windows.Forms.Button();
            this.seat2 = new System.Windows.Forms.Button();
            this.perde = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.filmler = new System.Windows.Forms.Label();
            this.bohemian_rhapsody = new System.Windows.Forms.Button();
            this.fight_club = new System.Windows.Forms.Button();
            this.titanic = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // seat1
            // 
            this.seat1.BackColor = System.Drawing.Color.LightGreen;
            this.seat1.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat1.Location = new System.Drawing.Point(309, 304);
            this.seat1.Name = "seat1";
            this.seat1.Size = new System.Drawing.Size(90, 90);
            this.seat1.TabIndex = 0;
            this.seat1.Text = "Koltuk 1";
            this.seat1.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.GhostWhite;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.seat14);
            this.panel1.Controls.Add(this.seat13);
            this.panel1.Controls.Add(this.seat12);
            this.panel1.Controls.Add(this.seat8);
            this.panel1.Controls.Add(this.seat7);
            this.panel1.Controls.Add(this.seat6);
            this.panel1.Controls.Add(this.seat5);
            this.panel1.Controls.Add(this.seat11);
            this.panel1.Controls.Add(this.seat10);
            this.panel1.Controls.Add(this.seat9);
            this.panel1.Controls.Add(this.seat4);
            this.panel1.Controls.Add(this.seat3);
            this.panel1.Controls.Add(this.seat2);
            this.panel1.Controls.Add(this.perde);
            this.panel1.Controls.Add(this.seat1);
            this.panel1.Location = new System.Drawing.Point(553, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 439);
            this.panel1.TabIndex = 1;
            // 
            // seat14
            // 
            this.seat14.BackColor = System.Drawing.Color.LightGreen;
            this.seat14.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat14.Location = new System.Drawing.Point(117, 16);
            this.seat14.Name = "seat14";
            this.seat14.Size = new System.Drawing.Size(90, 90);
            this.seat14.TabIndex = 14;
            this.seat14.Text = "Koltuk 14";
            this.seat14.UseVisualStyleBackColor = false;
            // 
            // seat13
            // 
            this.seat13.BackColor = System.Drawing.Color.LightGreen;
            this.seat13.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat13.Location = new System.Drawing.Point(213, 16);
            this.seat13.Name = "seat13";
            this.seat13.Size = new System.Drawing.Size(90, 90);
            this.seat13.TabIndex = 13;
            this.seat13.Text = "Koltuk 13";
            this.seat13.UseVisualStyleBackColor = false;
            // 
            // seat12
            // 
            this.seat12.BackColor = System.Drawing.Color.LightGreen;
            this.seat12.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat12.Location = new System.Drawing.Point(21, 112);
            this.seat12.Name = "seat12";
            this.seat12.Size = new System.Drawing.Size(90, 90);
            this.seat12.TabIndex = 12;
            this.seat12.Text = "Koltuk 12";
            this.seat12.UseVisualStyleBackColor = false;
            // 
            // seat8
            // 
            this.seat8.BackColor = System.Drawing.Color.LightGreen;
            this.seat8.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat8.Location = new System.Drawing.Point(21, 208);
            this.seat8.Name = "seat8";
            this.seat8.Size = new System.Drawing.Size(90, 90);
            this.seat8.TabIndex = 11;
            this.seat8.Text = "Koltuk 8";
            this.seat8.UseVisualStyleBackColor = false;
            // 
            // seat7
            // 
            this.seat7.BackColor = System.Drawing.Color.LightGreen;
            this.seat7.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat7.Location = new System.Drawing.Point(117, 208);
            this.seat7.Name = "seat7";
            this.seat7.Size = new System.Drawing.Size(90, 90);
            this.seat7.TabIndex = 10;
            this.seat7.Text = "Koltuk 7";
            this.seat7.UseVisualStyleBackColor = false;
            // 
            // seat6
            // 
            this.seat6.BackColor = System.Drawing.Color.LightGreen;
            this.seat6.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat6.Location = new System.Drawing.Point(213, 208);
            this.seat6.Name = "seat6";
            this.seat6.Size = new System.Drawing.Size(90, 90);
            this.seat6.TabIndex = 9;
            this.seat6.Text = "Koltuk 6";
            this.seat6.UseVisualStyleBackColor = false;
            // 
            // seat5
            // 
            this.seat5.BackColor = System.Drawing.Color.LightGreen;
            this.seat5.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat5.Location = new System.Drawing.Point(309, 208);
            this.seat5.Name = "seat5";
            this.seat5.Size = new System.Drawing.Size(90, 90);
            this.seat5.TabIndex = 8;
            this.seat5.Text = "Koltuk 5";
            this.seat5.UseVisualStyleBackColor = false;
            // 
            // seat11
            // 
            this.seat11.BackColor = System.Drawing.Color.LightGreen;
            this.seat11.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat11.Location = new System.Drawing.Point(117, 112);
            this.seat11.Name = "seat11";
            this.seat11.Size = new System.Drawing.Size(90, 90);
            this.seat11.TabIndex = 7;
            this.seat11.Text = "Koltuk 11";
            this.seat11.UseVisualStyleBackColor = false;
            // 
            // seat10
            // 
            this.seat10.BackColor = System.Drawing.Color.LightGreen;
            this.seat10.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat10.Location = new System.Drawing.Point(213, 112);
            this.seat10.Name = "seat10";
            this.seat10.Size = new System.Drawing.Size(90, 90);
            this.seat10.TabIndex = 6;
            this.seat10.Text = "Koltuk 10";
            this.seat10.UseVisualStyleBackColor = false;
            // 
            // seat9
            // 
            this.seat9.BackColor = System.Drawing.Color.LightGreen;
            this.seat9.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat9.Location = new System.Drawing.Point(309, 112);
            this.seat9.Name = "seat9";
            this.seat9.Size = new System.Drawing.Size(90, 90);
            this.seat9.TabIndex = 5;
            this.seat9.Text = "Koltuk 9";
            this.seat9.UseVisualStyleBackColor = false;
            // 
            // seat4
            // 
            this.seat4.BackColor = System.Drawing.Color.LightGreen;
            this.seat4.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat4.Location = new System.Drawing.Point(21, 304);
            this.seat4.Name = "seat4";
            this.seat4.Size = new System.Drawing.Size(90, 90);
            this.seat4.TabIndex = 4;
            this.seat4.Text = "Koltuk 4";
            this.seat4.UseVisualStyleBackColor = false;
            // 
            // seat3
            // 
            this.seat3.BackColor = System.Drawing.Color.LightGreen;
            this.seat3.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat3.Location = new System.Drawing.Point(117, 304);
            this.seat3.Name = "seat3";
            this.seat3.Size = new System.Drawing.Size(90, 90);
            this.seat3.TabIndex = 3;
            this.seat3.Text = "Koltuk 3";
            this.seat3.UseVisualStyleBackColor = false;
            // 
            // seat2
            // 
            this.seat2.BackColor = System.Drawing.Color.LightGreen;
            this.seat2.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seat2.Location = new System.Drawing.Point(213, 304);
            this.seat2.Name = "seat2";
            this.seat2.Size = new System.Drawing.Size(90, 90);
            this.seat2.TabIndex = 2;
            this.seat2.Text = "Koltuk 2";
            this.seat2.UseVisualStyleBackColor = false;
            // 
            // perde
            // 
            this.perde.AutoSize = true;
            this.perde.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.perde.Location = new System.Drawing.Point(178, 410);
            this.perde.Name = "perde";
            this.perde.Size = new System.Drawing.Size(57, 22);
            this.perde.TabIndex = 1;
            this.perde.Text = "Perde";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.titanic);
            this.panel2.Controls.Add(this.fight_club);
            this.panel2.Controls.Add(this.bohemian_rhapsody);
            this.panel2.Controls.Add(this.filmler);
            this.panel2.Location = new System.Drawing.Point(291, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(256, 439);
            this.panel2.TabIndex = 2;
            // 
            // filmler
            // 
            this.filmler.AutoSize = true;
            this.filmler.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filmler.Location = new System.Drawing.Point(47, 16);
            this.filmler.Name = "filmler";
            this.filmler.Size = new System.Drawing.Size(160, 22);
            this.filmler.TabIndex = 1;
            this.filmler.Text = "Vizyondaki Filmler";
            // 
            // bohemian_rhapsody
            // 
            this.bohemian_rhapsody.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bohemian_rhapsody.BackgroundImage")));
            this.bohemian_rhapsody.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bohemian_rhapsody.Location = new System.Drawing.Point(3, 54);
            this.bohemian_rhapsody.Name = "bohemian_rhapsody";
            this.bohemian_rhapsody.Size = new System.Drawing.Size(117, 186);
            this.bohemian_rhapsody.TabIndex = 2;
            this.bohemian_rhapsody.UseVisualStyleBackColor = true;
            // 
            // fight_club
            // 
            this.fight_club.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fight_club.BackgroundImage")));
            this.fight_club.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fight_club.Location = new System.Drawing.Point(134, 54);
            this.fight_club.Name = "fight_club";
            this.fight_club.Size = new System.Drawing.Size(117, 186);
            this.fight_club.TabIndex = 3;
            this.fight_club.UseVisualStyleBackColor = true;
            // 
            // titanic
            // 
            this.titanic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titanic.BackgroundImage")));
            this.titanic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.titanic.Location = new System.Drawing.Point(71, 246);
            this.titanic.Name = "titanic";
            this.titanic.Size = new System.Drawing.Size(117, 186);
            this.titanic.TabIndex = 4;
            this.titanic.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1000, 600);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "Main";
            this.Text = "Sinema Bilet Sistemi";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button seat1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label perde;
        private System.Windows.Forms.Button seat14;
        private System.Windows.Forms.Button seat13;
        private System.Windows.Forms.Button seat12;
        private System.Windows.Forms.Button seat8;
        private System.Windows.Forms.Button seat7;
        private System.Windows.Forms.Button seat6;
        private System.Windows.Forms.Button seat5;
        private System.Windows.Forms.Button seat11;
        private System.Windows.Forms.Button seat10;
        private System.Windows.Forms.Button seat9;
        private System.Windows.Forms.Button seat4;
        private System.Windows.Forms.Button seat3;
        private System.Windows.Forms.Button seat2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label filmler;
        private System.Windows.Forms.Button bohemian_rhapsody;
        private System.Windows.Forms.Button fight_club;
        private System.Windows.Forms.Button titanic;
    }
}

