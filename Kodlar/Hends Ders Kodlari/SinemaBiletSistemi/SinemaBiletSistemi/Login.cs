﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinemaBiletSistemi
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void login_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main main =new Main();
            main.FormClosed += new FormClosedEventHandler(main_FormClosed);
            main.Show();
        }
        private void main_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
    }
}
