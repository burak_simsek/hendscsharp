﻿
namespace PhotoSlider
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addnewmovie = new System.Windows.Forms.Button();
            this.filmcombobox = new System.Windows.Forms.ComboBox();
            this.idbox = new System.Windows.Forms.TextBox();
            this.namebox = new System.Windows.Forms.TextBox();
            this.directorbox = new System.Windows.Forms.TextBox();
            this.moviephotobox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.getmovielist = new System.Windows.Forms.Button();
            this.picturemovie = new System.Windows.Forms.PictureBox();
            this.filmname = new System.Windows.Forms.Label();
            this.filmdirector = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picturemovie)).BeginInit();
            this.SuspendLayout();
            // 
            // addnewmovie
            // 
            this.addnewmovie.BackColor = System.Drawing.Color.SandyBrown;
            this.addnewmovie.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addnewmovie.Location = new System.Drawing.Point(23, 214);
            this.addnewmovie.Name = "addnewmovie";
            this.addnewmovie.Size = new System.Drawing.Size(115, 91);
            this.addnewmovie.TabIndex = 0;
            this.addnewmovie.Text = "Add New Movie";
            this.addnewmovie.UseVisualStyleBackColor = false;
            this.addnewmovie.Click += new System.EventHandler(this.addnewmovie_Click);
            // 
            // filmcombobox
            // 
            this.filmcombobox.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filmcombobox.FormattingEnabled = true;
            this.filmcombobox.Location = new System.Drawing.Point(536, 42);
            this.filmcombobox.Name = "filmcombobox";
            this.filmcombobox.Size = new System.Drawing.Size(228, 30);
            this.filmcombobox.TabIndex = 1;
            this.filmcombobox.SelectedIndexChanged += new System.EventHandler(this.filmcombobox_SelectedIndexChanged);
            // 
            // idbox
            // 
            this.idbox.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idbox.Location = new System.Drawing.Point(152, 32);
            this.idbox.Name = "idbox";
            this.idbox.Size = new System.Drawing.Size(199, 33);
            this.idbox.TabIndex = 2;
            // 
            // namebox
            // 
            this.namebox.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namebox.Location = new System.Drawing.Point(152, 73);
            this.namebox.Name = "namebox";
            this.namebox.Size = new System.Drawing.Size(199, 33);
            this.namebox.TabIndex = 3;
            // 
            // directorbox
            // 
            this.directorbox.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directorbox.Location = new System.Drawing.Point(152, 118);
            this.directorbox.Name = "directorbox";
            this.directorbox.Size = new System.Drawing.Size(199, 33);
            this.directorbox.TabIndex = 4;
            this.directorbox.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // moviephotobox
            // 
            this.moviephotobox.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moviephotobox.Location = new System.Drawing.Point(152, 161);
            this.moviephotobox.Name = "moviephotobox";
            this.moviephotobox.Size = new System.Drawing.Size(199, 33);
            this.moviephotobox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 26);
            this.label1.TabIndex = 6;
            this.label1.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 7;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "Director";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 26);
            this.label4.TabIndex = 9;
            this.label4.Text = "MoviePhoto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(576, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 26);
            this.label5.TabIndex = 10;
            this.label5.Text = "Film Lists";
            // 
            // getmovielist
            // 
            this.getmovielist.BackColor = System.Drawing.Color.SkyBlue;
            this.getmovielist.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getmovielist.Location = new System.Drawing.Point(23, 311);
            this.getmovielist.Name = "getmovielist";
            this.getmovielist.Size = new System.Drawing.Size(115, 91);
            this.getmovielist.TabIndex = 11;
            this.getmovielist.Text = "Get Movie List";
            this.getmovielist.UseVisualStyleBackColor = false;
            this.getmovielist.Click += new System.EventHandler(this.getmovielist_Click);
            // 
            // picturemovie
            // 
            this.picturemovie.Location = new System.Drawing.Point(536, 80);
            this.picturemovie.Name = "picturemovie";
            this.picturemovie.Size = new System.Drawing.Size(228, 297);
            this.picturemovie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picturemovie.TabIndex = 13;
            this.picturemovie.TabStop = false;
            // 
            // filmname
            // 
            this.filmname.AutoSize = true;
            this.filmname.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filmname.Location = new System.Drawing.Point(594, 380);
            this.filmname.Name = "filmname";
            this.filmname.Size = new System.Drawing.Size(117, 26);
            this.filmname.TabIndex = 14;
            this.filmname.Text = "Film Name";
            // 
            // filmdirector
            // 
            this.filmdirector.AutoSize = true;
            this.filmdirector.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filmdirector.Location = new System.Drawing.Point(576, 406);
            this.filmdirector.Name = "filmdirector";
            this.filmdirector.Size = new System.Drawing.Size(138, 26);
            this.filmdirector.TabIndex = 15;
            this.filmdirector.Text = "Film Director";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 539);
            this.Controls.Add(this.filmdirector);
            this.Controls.Add(this.filmname);
            this.Controls.Add(this.picturemovie);
            this.Controls.Add(this.getmovielist);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.moviephotobox);
            this.Controls.Add(this.directorbox);
            this.Controls.Add(this.namebox);
            this.Controls.Add(this.idbox);
            this.Controls.Add(this.filmcombobox);
            this.Controls.Add(this.addnewmovie);
            this.Name = "Form1";
            this.Text = "Film Makinesi";
            ((System.ComponentModel.ISupportInitialize)(this.picturemovie)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addnewmovie;
        private System.Windows.Forms.ComboBox filmcombobox;
        private System.Windows.Forms.TextBox idbox;
        private System.Windows.Forms.TextBox namebox;
        private System.Windows.Forms.TextBox directorbox;
        private System.Windows.Forms.TextBox moviephotobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button getmovielist;
        private System.Windows.Forms.PictureBox picturemovie;
        private System.Windows.Forms.Label filmname;
        private System.Windows.Forms.Label filmdirector;
    }
}

