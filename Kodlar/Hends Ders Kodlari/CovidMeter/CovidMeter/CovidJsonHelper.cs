﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovidMeter
{
    class CovidJsonHelper
    {
        /*
        "03/10/2020": {
        "date": "03/10/2020",
        "totalTests": "10711883",
        "totalCases": "323014",
        "totalDeaths": "8384",
        "totalIntensiveCare": "",
        "totalIntubated": "",
        "totalRecovered": "283868",
        "tests": "103219",
        "cases": "1502",
        "critical": "1470",
        "pneumoniaPercent": "%6,4",
        "deaths": "59",
        "recovered": "1211"
        }
         
         */
        public string date { get; set; }
        public string totalTests{ get; set; }
        public string totalCases { get; set; }
        public string totalDeaths { get; set; }
        public string totalIntensiveCare { get; set; }
        public string totalIntubated { get; set; }
        public string totalRecovered { get; set; }
        public string tests { get; set; }
        public string cases { get; set; }
        public string critical { get; set; }
        public string pneumoniaPercent { get; set; }
        public string deaths { get; set; }
        public string recovered { get; set; }
    }
}
