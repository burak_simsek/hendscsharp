﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CovidMeter
{
    class Covid
    {

        public string GetCovidData()
        {
            string url = Constant.CovidApiUrl;

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            string jsonData = "";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                jsonData = reader.ReadToEnd();
            }
            return jsonData;
        }

        public int GetCovidDataCount(string covidList)
        {
            JObject o = JObject.Parse(covidList);
            return o.Count;
        }
        public JObject GetCovidDataJSON(string covidList)
        {
            JObject o = JObject.Parse(covidList);
            return o;
        }
        public List<string> GetCovidDays(JObject covidJSON)
        {
            List<string> covidDays = new List<string>();
            foreach (var item in covidJSON)
            {
                covidDays.Add(item.Key);
            }
            return covidDays;
        }
        public CovidJsonHelper GetCovidTodayData(string _date)
        {
            JObject covidJSON = GetCovidDataJSON(GetCovidData());
            CovidJsonHelper covidDays = new CovidJsonHelper();
            var item = covidJSON[_date];
            covidDays.date = item["date"].ToString();
            covidDays.totalTests = item["totalTests"].ToString(); 
            covidDays.totalCases = item["totalPatients"].ToString();
            covidDays.totalDeaths = item["totalDeaths"].ToString(); 
            covidDays.totalIntensiveCare = item["totalIntensiveCare"].ToString();
            covidDays.totalIntubated = item["totalIntubated"].ToString();
            covidDays.totalRecovered = item["totalRecovered"].ToString();
            covidDays.tests = item["tests"].ToString();
            covidDays.cases = item["cases"].ToString();
            covidDays.critical = item["critical"].ToString();
            covidDays.pneumoniaPercent = item["pneumoniaPercent"].ToString();
            covidDays.deaths = item["deaths"].ToString();
            covidDays.recovered = item["recovered"].ToString();

            return covidDays;
        }
    }
}
