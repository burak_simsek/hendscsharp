﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CovidMeter
{
    public partial class CovidTurkey : Form
    {
        Covid covid = new Covid();
        List<string> covidDays;
        public CovidTurkey()
        {
            
            InitializeComponent();
            covidDays = covid.GetCovidDays(covid.GetCovidDataJSON(covid.GetCovidData()));
            SetDate();
            GetTodayData();
        }

        private void SetDate()
        {

            for (int i = 0; i < covidDays.Count; i++)
            {
                comboBox1.Items.Add(covidDays[i].ToString());
                comboBox1.SelectedIndex = i;
            }
        }
        public void GetTodayData()
        {
            var _item = covid.GetCovidTodayData(covidDays[covidDays.Count - 1]);
            date.Text = _item.date;
            totalTests.Text = _item.totalTests;
            totalCases.Text = _item.totalCases;
            totalDeaths.Text = _item.totalDeaths; 
            tests.Text = _item.tests;
            cases.Text = _item.cases;
            deaths.Text = _item.deaths;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString().Length > 0)
            {
                var _item = covid.GetCovidTodayData(covidDays[comboBox1.SelectedIndex]);
                date.Text = _item.date;
                totalTests.Text = _item.totalTests;
                totalCases.Text = _item.totalCases;
                totalDeaths.Text = _item.totalDeaths;
                tests.Text = _item.tests;
                cases.Text = _item.cases;
                deaths.Text = _item.deaths;
            }
        }
    }
}
