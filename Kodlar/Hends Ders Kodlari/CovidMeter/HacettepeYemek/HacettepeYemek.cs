﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HacettepeYemek
{

    public partial class HacettepeYemek : Form
    {
        Meal meal = new Meal();
        List<string> mealDays;
        Label[] labels;
        public HacettepeYemek()
        {
            InitializeComponent();
            mealDays = meal.GetMealDays();
            SetDate();
            GetMealToday();

        }
        private void SetDate()
        {

            for (int i = 0; i < mealDays.Count; i++)
            {
                comboBox1.Items.Add(mealDays[i].ToString());
                comboBox1.SelectedIndex = i;
            }
        }
        public void GetMealToday()
        {
            MealJsonHelper mealJsonHelper;
            mealJsonHelper = meal.GetMealToday();
            label3.Text = "Tarih : " + mealJsonHelper.date;
            label2.Text = "Kalori : " + mealJsonHelper.calori;
            labels = new Label[mealJsonHelper.meals.Length];
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i] = new Label();
                labels[i].Text = "Yemek " + (i + 1) + " : " + mealJsonHelper.meals[i];
                labels[i].Font = new Font("Arial", 14.25f);
                labels[i].TabIndex = 24 + i;
                labels[i].AutoSize = true;
                labels[i].Location = new Point(10, 70 + 25 * (i + 1));
                labels[i].Size = new Size(300, 20);
                Controls.Add(labels[i]);
            }
            label2.Location = new Point(10, 70 + 25 * (labels.Length + 1));
            panel1.Location = new Point(10, 70 + 25 * (labels.Length + 2));
            this.Size = new Size(445, panel1.Height + panel1.Location.Y + 50);
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
        }
        public void GetMealByDate(string _date)
        {
            for (int i = 0; i < labels.Count(); i++)
            {
                this.Controls.Remove(labels[i]);
            }
            MealJsonHelper mealJsonHelper;
            mealJsonHelper = meal.GetMealDate(_date);
            label3.Text = "Tarih : " + mealJsonHelper.date;
            label2.Text = "Kalori : " + mealJsonHelper.calori;
            labels = new Label[mealJsonHelper.meals.Length];
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i] = new Label();
                labels[i].Text = "Yemek " + (i + 1) + " : " + mealJsonHelper.meals[i];
                labels[i].Font = new Font("Arial", 14.25f);
                labels[i].TabIndex = 24 + i;
                labels[i].AutoSize = true;
                labels[i].Location = new Point(10, 70 + 25 * (i + 1));
                labels[i].Size = new Size(300, 20);
                Controls.Add(labels[i]);
            }
            label2.Location = new Point(10, 70 + 25 * (labels.Length + 1));
            panel1.Location = new Point(10, 70 + 25 * (labels.Length + 2));
            this.Size = new Size(445, panel1.Height + panel1.Location.Y + 50);
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;

        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString().Length > 0)
            {

                GetMealByDate(comboBox1.SelectedItem.ToString());
            }

        }
    }
}
