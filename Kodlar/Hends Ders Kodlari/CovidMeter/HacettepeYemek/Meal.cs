﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace HacettepeYemek
{
    class Meal
    {
        public string GetMealData()
        {
            string url = Constant.HacettepeYemekXmlUrl;

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            string xmlData = null;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                xmlData = reader.ReadToEnd();
            }
            return xmlData;
        }
        public JObject GetMealDataJSON(string mealList)
        {
            JObject o = JObject.Parse(mealList);
            return o;
        }


        public string XmlMealListToJSON(string _mealXml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_mealXml);

            string json = JsonConvert.SerializeXmlNode(doc);

            return json;
        }
        public MealJsonHelper GetMealToday()
        {
            string today = DateTime.Now.ToString("d.MM.yyyy");
            JObject mealJSON = GetMealDataJSON(XmlMealListToJSON(GetMealData()));
            MealJsonHelper mealDays = new MealJsonHelper();
            var mealDay = mealJSON["gunler"]["gun"];
            for (int i = 0; i < mealDay.Count(); i++)
            {
                if (mealDay[i]["tarih"].ToString().Contains(today))
                {
                    mealDays.date = mealDay[i]["tarih"].ToString();
                    mealDays.calori = mealDay[i]["kalori"].ToString();
                    mealDays.meals = new string[mealDay[i]["yemekler"]["yemek"].Count()];
                    for (int j = 0; j < mealDay[i]["yemekler"]["yemek"].Count(); j++)
                    { 
                        mealDays.meals[j] = mealDay[i]["yemekler"]["yemek"][j].ToString();
                    }
                }
            }

            return mealDays;
        }
        public MealJsonHelper GetMealDate(string _date)
        {
            JObject mealJSON = GetMealDataJSON(XmlMealListToJSON(GetMealData()));
            MealJsonHelper mealDays = new MealJsonHelper();
            var mealDay = mealJSON["gunler"]["gun"];
            for (int i = 0; i < mealDay.Count(); i++)
            {
                if (mealDay[i]["tarih"].ToString()==_date)
                {
                    mealDays.date = mealDay[i]["tarih"].ToString();
                    mealDays.calori = mealDay[i]["kalori"].ToString();
                    mealDays.meals = new string[mealDay[i]["yemekler"]["yemek"].Count()];
                    for (int j = 0; j < mealDay[i]["yemekler"]["yemek"].Count(); j++)
                    {
                        mealDays.meals[j] = mealDay[i]["yemekler"]["yemek"][j].ToString();
                    }
                }
            }

            return mealDays;
        }
        public List<string> GetMealDays()
        { 
            JObject mealJSON = GetMealDataJSON(XmlMealListToJSON(GetMealData()));
            List<string> mealDays = new List<string>();
            var mealDay = mealJSON["gunler"]["gun"];
            for (int i = 0; i < mealDay.Count(); i++)
            {
                mealDays.Add(mealDay[i]["tarih"].ToString());
            }
            return mealDays;
        }
    }
}
