﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form_Metro_Tema
{
    public partial class Form1 : MetroForm
    {
        public Form1()
        {
            InitializeComponent();
            metroComboBox1.Items.Add("Burak");
            metroComboBox1.Items.Add("Faruk");
            metroComboBox1.Items.Add("Ayda");
            metroComboBox1.Items.Add("Muammer");
            metroComboBox1.Items.Add("Engin");
            metroComboBox1.Items.Add("Peri");
            metroComboBox1.Items.Add("Ezgi");
            metroComboBox1.Items.Add("Sevil");
            metroComboBox1.Items.Add("Öykü");
        }
    }
}
